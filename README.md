# 👏 Pokemon - Catch em all

This i a projct made out of Heroku and Angular JS 14. The main usage of the application is to catch Pokémons and store them on you profile page. 

## ✌️ Description

### 🔥 About the application

Pokemon trainer is a small app used to catch Pokémons and store them on you profilepage.

The application contains the following functions/components:

* Login / Create user - Stored in Heroku and localsession.

* Component to render Pokémons from API. You can "catch" them and store these on you profile.

* Profile page where you can find your catched Pokémons. You are able to set them free as well.

* Logout - When logout, the Pokemon trainer is i cleared from localsession and redirected to loginpage. 

### 🤖 Technologies

* HTML5
* JavaScript
* Angular JS (14)
* Tailwind
* Heroku

### ⚠️ Dependencies

* We recommend to not change the versions in the dependencies. Otherwise, the application might not work. 

### ⚡ Installing

Follow the steps below to create and setup the translation application.

1. Get an API-key at Heroku(https://www.heroku.com/)
2. Clone the repo:
```
git clone https://gitlab.com/noroff1/ng-pokemon.git
``` 
3. Install NPM packages:
```
npm install
```
4. Create and enter your API key:

Start by creating a folder in src called: enviroments
Create enviroments.ts and enviroments.prod.ts

enviroments.ts:
```
export const environment = {
  production: false,
  apiPokemon: "YOUR POKEMON API ENDPOINT*",
  apiTrainer: "YOUR HEROKU USER API ENDPOINT",
  apiPicture: "YOUR POKEMON IMAGE API ENDPOINT",
  apiKey: "YOUR API-KEY"
};
```

enviroments.prod.ts:
```
export const environment = {
  production: true,
  apiPokemon: "YOUR POKEMON API ENDPOINT*",
  apiTrainer: "YOUR HEROKU USER API ENDPOINT",
  apiPicture: "YOUR POKEMON IMAGE API ENDPOINT",
  apiKey: "YOUR API-KEY"
};
```

### 💻 Executing program

How to run the application:
```
ng serve
```

## 😎 Authors

Contributors and contact information

Simon Roshäll 
[@simonroshall](https://gitlab.com/simonroshall/)
Philip Hjelmberg 
[@PhilipHjelmberg](https://gitlab.com/PhilipHjelmberg)

## 🦝 Version History

* 0.1
    * Initial Release

## 🌌 License

This project is fully free for use.
