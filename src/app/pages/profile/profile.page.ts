import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { PokemonCatalogueService } from 'src/app/services/pokemon.services';
import { UserService } from 'src/app/services/user.services';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.css']
})
export class ProfilePage implements OnInit {

  get user(): User | undefined {
    return this.userService.user
  }

  constructor(
    private readonly userService: UserService,
    private readonly pokemonCatalogueService: PokemonCatalogueService
    ) { }

  ngOnInit(): void {
    this.pokemonCatalogueService.getAllPokemon()
  }

  get addPokemonImage(): Pokemon[] | [] {
    if (this.userService.user && this.userService.user?.pokemon.length > 0) {
        const pokemonWithImg = this.userService.user?.pokemon.map<any>(aPokemon => {
            console.log(aPokemon)
            return this.pokemonCatalogueService.pokemon?.find(poke => poke.name === aPokemon)
        })
        return pokemonWithImg;
    }
    return [];
}

}
