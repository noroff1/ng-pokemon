import { Injectable } from "@angular/core";
import { StorageKeys } from "../enums/storage-keys.enum";
import { Pokemon } from "../models/pokemon.model";
import { User } from "../models/user.model";
import { StorageUtil } from "../utils/storage.util";
import { PokemonCatalogueService } from "./pokemon.services";

@Injectable({
    providedIn: 'root'
})

export class UserService {

    private _user?: User;

    public get user(): User | undefined {
        return this._user;
    }

    set user(user: User | undefined) {
        StorageUtil.storageSave<User>(StorageKeys.User, user!)
        this._user = user
    }

    constructor() {
        this._user = StorageUtil.storageRead<User>(StorageKeys.User);
        
    }

    // user.pokemon.includes(pokemon.name)
    public isPokemonCaught(pokemonName: string): boolean {
        if (this._user) {
            return Boolean(this._user.pokemon.includes(pokemonName))
        }
        return false;
    }
    //removes a pokemon from users list
    public removePokemon(pokemonName:string): void {
        if (this._user) {
            this._user.pokemon = this._user.pokemon.filter((pokemon: string) => pokemon !== pokemonName);
        }
    }
    //adds a pokemon to users list
    public addPokemon(pokemonName:string): void {
        if (this._user) {
            this._user.pokemon.push(pokemonName)
        }
    }
}