import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { find, Observable, tap } from "rxjs";
import { environment } from "src/environments/environment";
import { Pokemon } from "../models/pokemon.model";
import { User } from "../models/user.model";
import { PokemonCatalogueService } from "./pokemon.services";
import { UserService } from "./user.services";

const { apiKey, apiTrainer } = environment

@Injectable({
    providedIn: 'root'
})
export class CatchService {
    constructor(
        private readonly pokemonCatalogueService: PokemonCatalogueService,
        private readonly http: HttpClient,
        private readonly userService: UserService
    ){ }

    //Adds a Pokemon to the API
    public addPokemon(id: string): Observable<User> {
        if (!this.userService.user) {
            throw new Error("addPokemon: No user")
        }

        const user: User = this.userService.user;
        
        const pokemon: Pokemon |undefined = this.pokemonCatalogueService.getPokemonById(id)
        
        if (!pokemon){
            throw new Error("addPokemon: no pokemon with id: " + id)
        }

        const headers = new HttpHeaders({
            'content-type': 'application/json',
            'x-api-key': apiKey
        })

        return this.http.patch<User>(`${apiTrainer}/${user?.id}`, {
            pokemon: [...user.pokemon, pokemon.name]
        },{
            headers
        })
        .pipe(
            tap((updatedUser:User) => {
                this.userService.user = updatedUser
            })
        )
    }

    //Removes a pokemon from the API 
    public removePokemonByName(pokemonName: string): Observable<User> {
        if (!this.userService.user) {
            throw new Error("addPokemon: No user")
        }

        const user: User = this.userService.user;
        
        const pokemon: Pokemon |undefined = this.pokemonCatalogueService.getPokemonByName(pokemonName)
        
        if (!pokemon){
            throw new Error("addPokemon: no pokemon with id: " + pokemonName)
        }

        if (this.userService.isPokemonCaught(pokemonName)) {
            this.userService.removePokemon(pokemonName)
        }
     
        const headers = new HttpHeaders({
            'content-type': 'application/json',
            'x-api-key': apiKey
        })

        return this.http.patch<User>(`${apiTrainer}/${user?.id}`, {
            pokemon: [...user.pokemon]
        },{
            headers
        })
        .pipe(
            tap((updatedUser:User) => {
                this.userService.user = updatedUser
            })
        )
    }
}