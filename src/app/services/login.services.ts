import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map, Observable, of, switchMap } from "rxjs";
import { environment } from "src/environments/environment";
import { User } from '../models/user.model'


const { apiTrainer, apiKey } = environment; 

@Injectable({
    providedIn: 'root'
})

export class LoginService {

    constructor(private readonly http: HttpClient) { }

    // Login with a model to handle form
    public login(username: string): Observable<User> {
        return this.checkUsername(username)
            .pipe(
                switchMap((user: User | undefined) => {
                    if (user === undefined) {
                        return this.createUser(username);
                    }
                    return of(user);
                })
            )
    }

    // Do user exist
    private checkUsername(username: string): Observable<User | undefined> {
        return this.http.get<User[]>(`${apiTrainer}?username=${username}`)
        .pipe(
            map((respose: User[]) => {
                return respose.pop();
            })
        )
    }

    // If not - Create user
    private createUser(username: string): Observable<User> {
        const user = {
            username,
            pokemon: []
        };
        const headers = new HttpHeaders({
            "Content-Type": "application/json",
            "x-api-key": apiKey
        });
        return this.http.post<User>(apiTrainer, user, {
            headers
        })

    }
}