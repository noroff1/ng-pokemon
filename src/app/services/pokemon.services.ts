import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { finalize, map } from "rxjs";
import { environment } from "src/environments/environment";
import { StorageKeys } from "../enums/storage-keys.enum";
import { Pokemon } from "../models/pokemon.model";
import { StorageUtil } from "../utils/storage.util";

const { apiPokemon, apiPicture } = environment;

@Injectable({
    providedIn: 'root'
})
export class PokemonCatalogueService {
    
    private _pokemon: Pokemon[] = []
    private _error: string = ""
    private _loading: boolean = false

    get pokemon(): Pokemon[] {
        return this._pokemon;
    }

    get error(): string {
        return this._error;
    }

    get loading(): boolean {
        return this._loading;
    }

    constructor(private readonly http: HttpClient ){}
    
//Extracting the id from the url.
    public getId(url:string): string {
        let words = url.split('/')
        let id = words[words.length -2]
        return id;
    }

    //get pokemon from the API and store them in sessionStorage
public getAllPokemon(): void {
    this._loading = true;
    this.http.get<Pokemon[]>(apiPokemon)
    .pipe(
        map((x: any) => x.results),
       )

        .subscribe({
            next: (pokemon: Pokemon[]) => {
                this._pokemon = pokemon; 
                this._pokemon.map(pokemon => pokemon.id = this.getId(pokemon.url))
                this._pokemon.map(pokemon => pokemon.picture = apiPicture + pokemon.id + ".png")
                //Store pokemon in sessionStorage.
                StorageUtil.storageSave<Pokemon>(StorageKeys.Pokemon, pokemon)
            },
            error: (error: HttpErrorResponse) => {
                this._error = error.message;
            }
        })
    }

public getPokemonById(id: string): Pokemon | undefined {
    return this._pokemon.find((pokemon:Pokemon) => pokemon.id === id);
}

public getPokemonByName(name: string): Pokemon | undefined {
    return this._pokemon.find((pokemon:Pokemon) => pokemon.name === name);
}
}