import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginPage } from './pages/login/login.page';
import { PokemonCharactersPage } from './pages/pokemon-characters/pokemon-characters.page';
import { ProfilePage } from './pages/profile/profile.page';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { PokemonListComponent } from './components/pokemon-list/pokemon-list.component';
import { PokemonListItemComponent } from './components/pokemon-list-item/pokemon-list-item.component';
import { CatchPokemonButtonComponent } from './components/catch-pokemon-button/catch-pokemon-button.component';
import { ProfilePokemonListComponent } from './components/profile-pokemon-list/profile-pokemon-list.component';
import { ProfilePokemonListItemComponent } from './components/profile-pokemon-list-item/profile-pokemon-list-item.component';
import { ReleasePokemonButtonComponent } from './components/release-pokemon-button/release-pokemon-button.component';
import { NavbarComponent } from './components/navbar/navbar.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginPage,
    PokemonCharactersPage,
    ProfilePage,
    LoginFormComponent,
    PokemonListComponent,
    PokemonListItemComponent,
    CatchPokemonButtonComponent,
    ProfilePokemonListComponent,
    ProfilePokemonListItemComponent,
    ReleasePokemonButtonComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
