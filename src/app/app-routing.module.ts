import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guards';
import { LoginPage } from './pages/login/login.page';
import { PokemonCharactersPage } from './pages/pokemon-characters/pokemon-characters.page';
import { ProfilePage } from './pages/profile/profile.page';

const routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    redirectTo: "/login",
  },
  {
    path: "login",
    component: LoginPage,
  },
  {
    path: "pokemons",
    component: PokemonCharactersPage,
    canActivate: [ AuthGuard ]
  },
  {
    path: "profile",
    component: ProfilePage,
    canActivate: [ AuthGuard ]    
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
