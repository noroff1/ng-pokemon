import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { CatchService } from 'src/app/services/catch.services';
import { UserService } from 'src/app/services/user.services';

@Component({
  selector: 'app-release-pokemon-button',
  templateUrl: './release-pokemon-button.component.html',
  styleUrls: ['./release-pokemon-button.component.css'],


})
export class ReleasePokemonButtonComponent implements OnInit {

  public isTaken: boolean = false 
  @Input() pokemonName: string = "";

  constructor(
    private readonly catchService: CatchService,
    private readonly userService: UserService
    ) { }

  ngOnInit(): void {
  }

  //onClick function to release pokemon from profile
  onReleaseClick(): void {
      this.catchService.removePokemonByName(this.pokemonName)
      .subscribe({
        next: (user: User) => {

            console.log("NEXT:", user)
           this.isTaken = this.userService.isPokemonCaught(this.pokemonName);
        },
        error:(error:HttpErrorResponse) => {
            console.log("Error", error.message)
        }
      })
    }
  }



