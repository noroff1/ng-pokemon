import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-profile-pokemon-list-item',
  templateUrl: './profile-pokemon-list-item.component.html',
  styleUrls: ['./profile-pokemon-list-item.component.css']
})
export class ProfilePokemonListItemComponent implements OnInit {

  @Input() onePokemon!: Pokemon


  constructor() { }

  ngOnInit(): void {
  }

}
