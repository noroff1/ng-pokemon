import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { UserService } from 'src/app/services/user.services';

@Component({
  selector: 'app-profile-pokemon-list',
  templateUrl: './profile-pokemon-list.component.html',
  styleUrls: ['./profile-pokemon-list.component.css']
})
export class ProfilePokemonListComponent implements OnInit {

  @Input() pokemon:Pokemon[] | undefined = [];

  constructor() { }

  ngOnInit(): void {
  }

}
