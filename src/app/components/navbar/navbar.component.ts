import { Component, OnInit } from '@angular/core';
import { Routes } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.services';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  // logo: '../../assets/images/pokemonLogo.png',
})
export class NavbarComponent implements OnInit {

  get user(): User | undefined {
    return this.userService.user;
  } 

 

  

  constructor(
    private readonly userService: UserService
  ) { }

  logout() {
    // Remove user sessionStorage and rerout to login
    sessionStorage.removeItem('pokemon-user');
    this.userService.user = undefined;
  }

  ngOnInit(): void {
  }

}
