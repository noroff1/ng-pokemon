import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { CatchService } from 'src/app/services/catch.services';
import { UserService } from 'src/app/services/user.services';

@Component({
  selector: 'app-catch-pokemon-button',
  templateUrl: './catch-pokemon-button.component.html',
  styleUrls: ['./catch-pokemon-button.component.css']
})
export class CatchPokemonButtonComponent implements OnInit {

  public isCaught: boolean = false;

  @Input() pokemonId: string = "";
  @Input() pokemonName: string = "";

  constructor(
    private readonly catchService: CatchService,
    private readonly userService: UserService
  ) { }

  ngOnInit(): void {
    this.isCaught = this.userService.isPokemonCaught(this.pokemonName);
  }

  //Function to add a pokemon to the users pokemonlist when the button Catch is clicked
  onCatchClick(): void {
    this.catchService.addPokemon(this.pokemonId)
      .subscribe({
        next: (response: any) => {
          this.isCaught = this.userService.isPokemonCaught(this.pokemonName);
          console.log("NEXT ", response);
          console.log(this.pokemonName)
        },
        error: (error: HttpErrorResponse) => {
          console.log("ERROR ", error.message);
        }
      })
  }
}
