// Object types of user
export interface User {
    id: number;
    username: string;
    pokemon: string[];
}