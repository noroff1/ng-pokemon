export interface Pokemon {
    name: string;
    id: string;
    url: string;
    picture: string; 
}